#!/bin/sh
set -xue

echo $KUBE_CONFIG | base64 -d > kube.config
echo $GOOGLE_APPLICATION_CREDENTIALS_DATA | base64 -d > application_default_credentials.json

./make-deployment-yaml.sh > deployment.yaml

ls -l kube.config
echo kube.config
cat ./kube.config
ls -l application_default_credentials.json
echo application_default_credentials.json
cat ./application_default_credentials.json

GOOGLE_APPLICATION_CREDENTIALS=application_default_credentials.json \
  kubectl apply --kubeconfig kube.config -f deployment.yaml --v=8

