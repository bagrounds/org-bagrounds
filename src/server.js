;(() => {
  'use strict'

  /* imports */
  const express = require('express')
  const helmet = require('helmet')
  const compression = require('compression')
  const fs = require('fs')
  const socketServer = require('./lib/socket-server')

  const typeFromPath = path => /\.css$/.test(path)
    ? 'text/css'
    : /\.js$/.test(path)
    ? 'text/javascript'
    : /\.html$/.test(path)
    ? 'text/html'
    : 'text'

  const setMime = (path, res) => {
    res.setHeader('content-type', typeFromPath(path))
    return res
  }

  const send = (path, res) => fs.createReadStream(path).pipe(setMime(path, res))

  const staticOptions = {
    maxAge: 60 * 60 * 24 * 365 * 1000
  }

  const handlers = [
    ['/', './public/index.html'],
    ['/games', './public/games.html']
  ]

  const server = express()
    .disable('x-powered-by')
    .use(compression())
    .use(express.static('public'))

  handlers.forEach(([url, path]) => server.get(url, (req, res) => send(path, res)))

  server.use(helmet())

  socketServer.start(server)

  module.exports = server
})()

