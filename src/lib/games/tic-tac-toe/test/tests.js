;(() => {
  'use strict'

  const funTest = require('fun-test')
  const { lift, compose, composeAll } = require('fun-function')
  const { equal } = require('fun-predicate')
  const { get } = require('fun-object')
  const { sequence, repeat, fold, map, flatten, zipWith, range } = require('fun-array')
  const { add } = require('fun-scalar')
  const curry = require('fun-curry')
  const gen = require('fun-generator')

  const sum = fold(add, 0)
  const log = x => { console.log(JSON.stringify(x)); return x }

  const differBy = (n, a1) => composeAll([
    equal(n),
    sum,
    map(n => n ? 0 : 1),
    zipWith(equal, a1)
  ])

  const moves = n => map(
    gen.tuple([
      gen.tuple([gen.member([1]), gen.member(range(0, 8))]),
      gen.record({
        turn: gen.member([0]),
        board: gen.arrayOf(gen.member([0]))
      })
    ]),
    sequence(() => [
      map(Math.random, repeat(2, 0)),
      {
        turn: Math.random(),
        board: map(Math.random, repeat(9, 0)),
      }
    ], n)
  )

  const tests = map(funTest.sync, flatten([
    [
      {
        inputs: [],
        predicate: equal(true),
        contra: lift(compose)(get('validState'), get('init'))
      }
    ],
    map(([move, state]) => ({
      inputs: [move, state],
      predicate: compose(differBy(1, state.board), get('board')),
      contra: get('move')
    }), moves(10)),
    map(([move, state]) => ({
      inputs: [move, state],
      predicate: compose(equal(add(1, state.turn)), get('turn')),
      contra: get('move')
    }), moves(10))
  ]))

  module.exports = tests
})()
