;(() => {
  'use strict'

  module.exports = ({ all, vectorOf, member, or, equal }) => ({
    board: all([
      vectorOf(9, member([0, 1, 2])),
      composeAll([
        or(equal(0), equal(-1)),
        argsToArray(sub),
        ap([countBy(equal(1)), countBy(equal(2))]),
        repeat(2)
      ])
    ])
  })
})()
