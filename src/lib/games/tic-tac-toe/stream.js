;(() => {
  'use strict'

  const empty = () => []
  const of = x => [x, empty]
  const prepend = (x, s) => [x, () => s]
  const fromArray = xs => xs.reduceRight((s, x) => prepend(x, s), [])
  const map = (f, s) => !s.length ? s : [f(s[0]), () => map(f, s[1]())]
  const take = (n, s) => (!n || !s.length) ? [] : [s[0], ...take(n - 1, s[1]())]
  const iterate = (f, x) => [x, () => iterate(f, f(x))]
  const scan = (f, x, s) => !s.length ? [] : [f(x, s[0]), () => scan(f, f(x, s[0]), s[1]())]
  const filter = (f, s) => !s.length ? [] : f(s[0]) ? [s[0], () => filter(f, s[1]())] : filter(f, s[1]())
  const nth = (n, s) => !n ? s[0] : nth(n - 1, s[1]())

  const api = { empty, of, prepend, fromArray, map, take, iterate, scan, filter, nth }

  module.exports = api
})()
