;(() => {
  'use strict'

  const obj = require('fun-object')
  const { get, partition, fold, length, filter, ap, set, repeat, range } = require('fun-array')
  const { inputs, output } = require('../../../../../guarded')
  const curry = require('fun-curry')
  const { bool, record, vectorOf, tuple } = require('fun-type')
  const { gte, lte, add, sub, mod } = require('fun-scalar')
  const { equal, or, all, not, equalDeep } = require('fun-predicate')
  const { id, argsToArray, composeAll, compose } = require('fun-function')
  const arrange = require('fun-arrange')

  const member = xs => x => xs.reduce((a, b) => a || equalDeep(x, b), false)
  const sum = fold(add, 0)
  const log = x => { console.log(JSON.stringify(x)); return x }

  const countBy = p => compose(length, filter(x => p(x)))
  const validTurn = all([gte(0), lte(9)])

  const validBoard = all([
    vectorOf(9, member([0, 1, 2])),
    composeAll([
      or(equal(0), equal(-1)),
      argsToArray(sub),
      ap([countBy(equal(1)), countBy(equal(2))]),
      repeat(2)
    ])
  ])

  const validState = all([
    record({ turn: validTurn, board: validBoard }),
    composeAll([
      argsToArray(equal),
      ap([id, compose(length, filter(x => not(equal(0))(x)))]),
      arrange(['turn', 'board'])
    ])
  ])

  const validMove = tuple([member([1, 2]), member(range(0, 8))])

  const playerTurn = compose(
    argsToArray(equal),
    ap([compose(sub(1), get(0)), compose(mod(2), obj.get('turn'))]),
  )

  const positionAvailable = composeAll([
    equal(0),
    argsToArray(get),
    ap([get(1), obj.get('board')]),
  ])

  const validMoveForState = all([
    tuple([validMove, validState]),
    playerTurn,
    positionAvailable
  ])

  const init = () => ({ turn: 0, board: repeat(9, 0) })

  const move = ([player, position], state) => obj.update(
    'board',
    set(position, player),
    obj.update('turn', add(1), state)
  )

  const renderPos = a => a === 0 ? ' ' : a === 1 ? 'X' : a === 2 ? 'O' : '?'

  const renderRow = row => (([a, b, c]) => ` ${a} | ${b} | ${c}\n`)(row.map(renderPos))

  const render = ({ board: b }) =>
        renderRow(b.slice(0, 3)) +
    '-----------\n' +
        renderRow(b.slice(3, 6)) +
    '-----------\n' +
        renderRow(b.slice(6, 9))

  const api = { init, move, validMove, validState, validMoveForState, render }

  const guards = {
    init: output(validState),
    move: inputs(validMoveForState),
    validMove: output(bool),
    validState: output(bool)
  }

  module.exports = obj.map(curry, obj.ap(guards, api))
})()
