;(() => {
  'use strict'

  /* imports */
  const ws = require('ws')
  const uuid = require('uuid/v4')
  const array = require('fun-array')
  const obj = require('fun-object')
  const most = require('most')
  const funCase = require('fun-case')
  const type = require('fun-type')
  const fn = require('fun-function')
  const chatApp = require('./chat')

  const MESSAGE_TYPES = ['nameUser', 'sendMessage', 'parseFail']

  const log = fn.tee(x => console.log(x))

  const wrap = k => v => obj.of(k, v)
  const wraps = ks => fn.composeAll(ks.map(wrap))
  const wrapConnection = wrap('connection')
  const wrapClose = wrap('close')
  const wrapParseFail = wrap('parseFail')
  const wrapMessageOf = id => s => fn.composeAll([
    wraps(['message', s]),
    obj.merge({ id }),
    wrap('data'),
    obj.get(s)
  ])

  const poly = cases => fallThrough => funCase(
    array.append(
      { p: type.any, f: fallThrough },
      Object.keys(cases).map(key => ({
        p: type.record(obj.of(key, x => x !== undefined)),
        f: cases[key]
      }))
    )
  )

  const wrapMessage = id => poly(
    obj.ofPairs(MESSAGE_TYPES.map(mt => [mt, wrapMessageOf(id)(mt)]))
  )(m => { log(`Unable to wrap message: ${JSON.stringify(m)}`); return false })

  const transition = alg => poly({
    connection: ({ connection }) => obj.ap({
      sockets: obj.set(connection.id, connection.ws),
      users: obj.set(connection.id, connection.id)
    }),
    close: ({ close }) => obj.ap({
      sockets: obj.drop([close.id]),
      users: obj.drop([close.id])
    }),
    message: alg
  })(X => state => { log(`__unhandled  transition: ${JSON.stringify(X)}`); return state })

  const formatChat = names => ([i, chat]) => [names[i], chat]
  const tryParse = j => { try { return JSON.parse(j) } catch (e) { return wrapParseFail(j) } }
  const broadcast = sockets => data => obj.values(sockets)
    .forEach(s => s.send(data, error => error && console.error(error)))

  const build = app => () => {
    const accept = type.record(obj.of('message', app.accept))
    const initialState = obj.defaults(
      { sockets: {} },
      app.init(app.algebra.ops)(app.algebra.empty())
    )
    const fold = (state, m) => transition(
      fn.composeAll([
        poly(app.algebra.ops)(x => { throw Error(JSON.stringify(x)) }),
        obj.get('message'),
      ])
    )(m)(state)

    const socketServer = new ws.Server({ port: 8081 })

    most.fromEvent('error', socketServer).observe(log)

    const connections = most.fromEvent('connection', socketServer)
      .map(([ws]) => [uuid(), ws])

    connections
      .chain(([id, ws]) => most.fromEvent('error', ws).map(e => [id, e]))
      .observe(([id, e]) => console.log(`${id}: ${e}`))

    connections
      .chain(([id, ws]) => most.mergeArray([
        most.of(wrapConnection({ id, ws })),
        most.fromEvent('message', ws)
          .map(obj.get('data'))
          .map(tryParse)
          .map(wrapMessage(id))
          .filter(accept),
        most.fromEvent('close', ws)
          .map(e => wrapClose({ id }))
      ]))
      .scan(fold, initialState)
      .map(s => obj.ap({ chat: array.map(formatChat(s.users)) }, s))
      .map(s => ({ sockets: s.sockets, message: obj.drop(['sockets'], s) }))
      .map(obj.ap({ message: JSON.stringify }))
      .observe(({sockets, message}) => sockets && broadcast(sockets)(message))

    console.log('started socket server')
  }

  const start = build(chatApp)

  module.exports = { start }
})()
