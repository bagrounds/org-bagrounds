;(() => {
  'use strict'

  /* imports */
  const uuid = require('uuid/v4')
  const array = require('fun-array')
  const obj = require('fun-object')
  const funCase = require('fun-case')
  const type = require('fun-type')
  const fn = require('fun-function')
  const pred = require('fun-predicate')

  const MESSAGE_TYPES = ['name', 'chat', 'parseFail']

  const poly = cases => fallThrough => funCase(
    array.append(
      { p: type.any, f: fallThrough },
      Object.keys(cases).map(key => ({
        p: type.record(obj.of(key, x => x !== undefined)),
        f: cases[key]
      }))
    )
  )

  const isUuid = pred.and(
    type.string,
    s => /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/.test(s)
  )

  const formatChat = names => ([i, chat]) => [names[i], chat]

  module.exports = {
    accept: pred.or(
      type.record({
        nameUser: type.record({ id: isUuid, data: type.string })
      }),
      type.record({
        sendMessage: type.record({ id: isUuid, data: type.string })
      })
    ),
    broadcast: users => state => obj.values(users).map(u => ({ to: u, data: state })),
    parse: poly({
      nameUser: ({ nameUser }) => nameUser,
      sendMessage: ({ sendMessage }) => sendMessage,
    })(x => { throw Error(JSON.stringify(x)) }),
    init: ops => (id => fn.composeAll([
      ops.sendMessage({ sendMessage: { id, data: 'Beginning of chat'} }),
      ops.nameUser({ nameUser: { id, data: 'admin'} })
    ]))(uuid()),
    algebra: {
      empty: () => ({ users: {}, chat: [] }),
      ops: {
        nameUser: ({ nameUser }) => obj.ap({
          users: obj.set(nameUser.id, nameUser.data)
        }),
        sendMessage: ({ sendMessage }) => obj.ap({
          chat: array.append([sendMessage.id, sendMessage.data])
        })
      }
    }
  }
})()
