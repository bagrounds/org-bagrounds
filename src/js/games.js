;(() => {
  'use strict'

  const hyperapp = require('hyperapp')

  const qs = q => document.querySelector(q)
  const assign = (o, props) => Object.assign(o, props)
  const ce = (tag, props = {}, children = []) =>
    hyperapp.h(tag, props, children )

  const svg = (tag, atts) => {
    const s = document.createElementNS("http://www.w3.org/2000/svg", tag)
    Object.keys(atts).forEach(k => s.setAttribute(k, atts[k]))
    return s
  }

  window.onload = main

  const isPrimitive = x => typeof x === 'number' ||
    typeof x === 'string' ||
    typeof x === 'boolean' ||
    x == null

  const unsafeCopy = o => isPrimitive(o)
    ? o
    : o instanceof Array
    ? o.map(unsafeCopy)
    : o instanceof Object
    ? Object.keys(o)
        .reduce((result, k) => Object.assign(result, { [k]: unsafeCopy(o[k]) }), {})
    : o

  const event = type => data => ({ [type]: data })
  const eName = event('name')
  const ePoint = event('point')
  const eConnected = event('connected')(true)

  const canParse = j => { try { JSON.parse(j); return true } catch (e) { return false } }

  const sendWs = ws => msg => ws.send(
    JSON.stringify(msg),
    error => error && console.error(error)
  )

  const setEvents = ws => events => {
    Object.keys(events).forEach(e => ws[`on${e}`] = events[e])
    return ws
  }

  const tee = f => x => { f(x); return x }
  const log = tee(x => console.log(x))

  function main () {
    const host = location.hostname
    const ws = new WebSocket(`ws://${host}:8081`)

    const state = {
      users: {},
      chat: []
    }

    const copyInputToState = key => input => state => assign(unsafeCopy(state), { [key]: input })

    const actions = {
      updateUsers: copyInputToState('users'),
      updateName: e => tee(() => {
        (e.keyCode === 13) && sendWs(ws)(event('nameUser')(e.target.value))
      }),
      updateChat: copyInputToState('chat'),
      chat: e => tee(() => {
        if (e.keyCode === 13) {
          sendWs(ws)(event('sendMessage')(e.target.value))
          e.target.value = ''
        }
      })
    }

    const renderUsers = users => Object.keys(users)
      .map(k => ce('li', { class: 'user-name' }, [users[k]]))

    const chatLength = 20
    const repeat = n => x => Array.apply(null, { length: n }).map(() => x)
    const fillChat = chat => chat.length < chatLength
      ? repeat(20 - chat.length)('-').concat(chat)
      : chat.slice(0, chatLength)
    const renderChat = chat => fillChat(chat)
      .map(([name, msg]) => ce('li', { class: 'chat-line' }, [
        ce('span', { class: 'name' }, name + ':'),
        ce('span', { class: 'message' }, msg)
      ]))

    const view = (state, actions) => ce('main', {}, [
        ce('header', {}, [
          ce('h1', {}, ['Games!'])
        ]),
        ce('section', { class: 'info' }, [
          ce('div', { class: 'users' }, [
            ce('h2', { class: 'user-list-title' }, 'Users'),
            ce('ul', { class: 'user-list' }, renderUsers(state.users)),
            ce('input', { onkeyup: actions.updateName, placeholder: 'username' })
          ]),
          ce('div', { class: 'chat' }, [
            ce('h2', { class: 'chat-log-title' }, 'Chat'),
            ce('ul', { class: 'chat-log' }, renderChat(state.chat)),
            ce('input', { class: 'chat-input', onkeyup: actions.chat, placeholder: 'chat' }),
          ])
        ])
      ])

    const emit = hyperapp.app(state, actions, view, document.body)

    setEvents(ws)({
      open: () => {
        sendWs(ws)(eConnected)
      },
      message: ({ data }) => {
        if (canParse(data)) {
          (({ users, chat }) => {
            if (users) {
              emit.updateUsers(users)
            }
            if (chat) {
              emit.updateChat(chat)
            }
          })(JSON.parse(data))
        }
        else log(`can't parse message: ${JSON.stringify(data)}`)
      }
    })

    const canvas = svg('svg', {
      class: 'canvas'
    })
    canvas.addEventListener('click', e => {
      sendWs(ws)(ePoint([e.offsetX, e.offsetY]))
      log(`(${e.offsetX}, ${e.offsetY})`)
      const circle = svg('circle', {
        class: 'circle',
        cx: e.offsetX,
        cy: e.offsetY,
        r: 5
      })
      canvas.append(circle)
    })
    //game.append(canvas)
  }
})()
