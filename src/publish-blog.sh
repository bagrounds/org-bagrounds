#!/bin/sh

mkdir -p public/blog

cd public/blog

git archive --format=tar --remote=git@gitlab.com:bagrounds/org-bagrounds-blog HEAD | tar xf -

exit 0
