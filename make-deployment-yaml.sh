#!/bin/sh
set -u

IMAGE=registry.gitlab.com/bagrounds/org-bagrounds/www
VERSION=$(./bin/version)

cat <<EOF
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    run: org-bagrounds
  name: org-bagrounds
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      run: org-bagrounds
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        run: org-bagrounds
    spec:
      containers:
      - env:
        - name: NODE_ENV
          value: rate-limited
        image: $IMAGE:$VERSION
        imagePullPolicy: IfNotPresent
        name: org-bagrounds
        ports:
        - containerPort: 8080
          name: http
          protocol: TCP
        - containerPort: 8081
          name: ws
          protocol: TCP
        - containerPort: 8443
          name: https
          protocol: TCP
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
---
apiVersion: v1
kind: Service
metadata:
  labels:
    run: org-bagrounds
  name: org-bagrounds
  namespace: default
spec:
  externalTrafficPolicy: Cluster
  ports:
  - name: ws
    port: 8081
    protocol: TCP
    targetPort: 8081
  - name: http
    port: 80
    protocol: TCP
    targetPort: 8080
  - name: https
    port: 443
    protocol: TCP
    targetPort: 8443
  selector:
    run: org-bagrounds
  type: LoadBalancer
EOF
