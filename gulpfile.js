;(function () {
  'use strict'

  /* imports */
  var gulp = require('gulp')
  var stylus = require('gulp-stylus')
  var pug = require('gulp-pug')
  var del = require('del')
  var rename = require('gulp-rename')
  var pkg = require('./package.json')
  var data = require('./src/data.json')
  data.version = pkg.version

  var TASK = {}
  TASK.MAKE_CLEAN = {}
  TASK.MAKE_CLEAN.NAME = 'clean'
  TASK.MAKE_CLEAN.SOURCE = 'public'
  TASK.MAKE_CLEAN.DEFAULT = false
  TASK.MAKE_CLEAN.WATCH = false
  TASK.MAKE_CLEAN.FUNCTION = function makeClean () {
    return del([
      TASK.MAKE_CLEAN.SOURCE
    ])
  }

  TASK.MAKE_CSS = {}
  TASK.MAKE_CSS.NAME = 'css'
  TASK.MAKE_CSS.SOURCE = 'src/styles/*.styl'
  TASK.MAKE_CSS.OUTPUT = 'public/css'
  TASK.MAKE_CSS.DEFAULT = true
  TASK.MAKE_CSS.WATCH = true
  TASK.MAKE_CSS.FUNCTION = function makeCss () {
    return gulp.src(TASK.MAKE_CSS.SOURCE)
      .pipe(stylus({
        compress: false,
        paths: [
          'styles'
        ],
        compile: function (str, path) {
          return stylus(str)
            .set('filename', path)
        }
      }))
      .pipe(gulp.dest(TASK.MAKE_CSS.OUTPUT))
  }

  TASK.MAKE_HTML = {}
  TASK.MAKE_HTML.NAME = 'html'
  TASK.MAKE_HTML.SOURCE = 'src/views/*.pug'
  TASK.MAKE_HTML.OUTPUT = 'public'
  TASK.MAKE_HTML.DEFAULT = true
  TASK.MAKE_HTML.WATCH = true
  TASK.MAKE_HTML.FUNCTION = function makeHtml () {
    return gulp.src(TASK.MAKE_HTML.SOURCE)
      .pipe(pug({
        locals: data,
        pretty: true
      }))
      .pipe(gulp.dest(TASK.MAKE_HTML.OUTPUT))
  }

  var WATCH_TASKS = []
  var DEFAULT_TASKS = []

  // make an array of the above tasks
  var TASKS = Object.keys(TASK).map(function (taskKey) {
    return TASK[taskKey]
  })

  // create each gulp task
  TASKS.forEach(function (task) {
    if (task.DEFAULT) {
      DEFAULT_TASKS.push(task.NAME)
    }

    if (task.WATCH) {
      WATCH_TASKS.push(task)
    }

    gulp.task(task.NAME, task.FUNCTION)
  })

  gulp.task('default', DEFAULT_TASKS)

  gulp.task('watch', function watch () {
    WATCH_TASKS.forEach(function (task) {
      gulp.watch(task.SOURCE, [task.NAME])
    })
  })
})()

