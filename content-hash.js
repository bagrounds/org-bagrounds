#!/usr/bin/env node
/**
 * content-hash creates a hash of the content of a file
 *
 * @module content-hash
 */
;(function () {
  'use strict'

  /* imports */
  var hashFunction = require('hash-function')
  var fs = require('fs')
  var replaceFile = require('replacefile')

  var alpha = 'abcdefghijklmnopqrstuvwxyz'
  alpha += alpha.toUpperCase()
  alpha += '0123456789'
  alpha += '-_'

  var alphaMap = alpha.split('')

  /* exports */
  module.exports = contentHash

  var files = [
    ['public/css/stylesheet', '.css']
  ]

  files.forEach(([file, ext], i) => {
    contentHash({ file: file + ext }, (error, hash) => {
      console.log('hash: ' + hash)
      const oldFile = file + ext
      var newFile = `${file}-${hash + ext}`
      fs.rename(oldFile, newFile, function (error, result) {
        [
          'public/index.html',
          'public/games.html'
        ].forEach(
          f => replaceFile.replace(f, /stylesheet.css/, 'stylesheet-' + hash + ext)
        )
      })
    })
  })

  /**
   * contentHash creates a hash of the content of a file
   *
   * @function contentHash
   * @alias content-hash
   *
   * @param {Object} options all function parameters
   * @param {Function} callback handle results
   */
  function contentHash (options, callback) {
    var file = options.file
    console.log('file: ' + file)

    fs.readFile(file, function (error, content) {
      var string = content.toString().replace(/\s/g, '')

      var hashFunctionOptions = {
        string: string
      }

      hashFunction(hashFunctionOptions, function (error, n) {
        var mod
        var c0 = alphaMap[0]
        var digits = [c0, c0, c0, c0, c0, c0]

        var i = 5
        while (i >= 0) {
          if (n <= 0) {
            break
          }

          mod = n % 64
          digits[i] = alphaMap[mod]
          n -= mod
          n /= 64
          i--
        }

        callback(null, digits.join(''))
      })
    })
  }
})()

